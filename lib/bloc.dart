import 'dart:async';
import 'validator.dart';
import 'package:rxdart/rxdart.dart';
// with keyword for using mixins
//rxdart - for checking if data is there or not
//BehaviourSubject - will broadcast.. second subscriber will also listen
//StreamController = listens only once
class Bloc extends Object with validator implements BaseBloc{
  final _emailController = BehaviorSubject<String>();
  final _passwordController = BehaviorSubject<String>();
  Stream<String> get email => _emailController.stream.transform(emailValidator);
  Stream<String> get password => _passwordController.stream.transform(passwordValidator);

  Function(String) get emailChanged => _emailController.sink.add;
  Function(String) get passwordChanged => _passwordController.sink.add;
  Stream<bool> get submitCheck => Observable.combineLatest2(email, password, (email,password)=>true);

//  Stream<bool> get submitCheck => Observable.combineLatest2(email, password, (email,password)=>true);
  void dispose(){
    _emailController.close();
    _passwordController.close();
  }
}

abstract class BaseBloc{
  void dispose();
}