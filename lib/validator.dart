//mixin class
import 'dart:async';

mixin validator{
  var emailValidator = StreamTransformer<String,String>.fromHandlers(
    handleData: (email,sink){
      if(email.contains('@')){
        sink.add(email);
      }
      else{
        sink.addError("email is invalid");
      }
    }
  );
  var passwordValidator = StreamTransformer<String,String>.fromHandlers(
      handleData: (password,sink){
        if(password.length>4){
          sink.add(password);
        }
        else{
          sink.addError("password should be more than 4 chars");
        }
      }
  );
}